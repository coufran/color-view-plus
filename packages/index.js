import "./assets/icon.css";

import ColorView from "color-view";

const ColorViewPlus = {
    install: Vue => {
        Vue.use(ColorView);
        
        let requireComponent = require.context(
            "./components", // 其组件目录的相对路径
            false, // 是否查询其子目录
            /[A-Z]\w+\.(vue|js)$/ // 匹配基础组件文件名的正则表达式
        );
        
        requireComponent.keys().forEach(fileName => {
            let component = requireComponent(fileName).default;
            Vue.component(component.name, component);
        });
    }
};

export default ColorViewPlus;