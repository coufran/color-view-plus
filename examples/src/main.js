import Vue from "vue";
import VueRouter from "vue-router";
import ColorViewPlus from "../../packages";
import Example from "./Example.vue";
import routes from "./routes.js";

Vue.use(ColorViewPlus);
Vue.use(VueRouter);

Vue.config.productionTip = false;

new Vue({
    render: h => h(Example),
    router: new VueRouter({
        routes
    }),
    data() {
        return {
            user: {
                meta: {
                    username: "测试",
                    roles: ["APP_USER", "SERVICER"],
                    avatar: null,
                },
                operations: [{
                    label: "切换到",
                    operations: [{
                        label: "用户",
                        click: () => {
                            console.info("用户click");
                        }
                    }, {
                        label: "服务商",
                        click: () => {
                            console.info("服务商click");
                        }
                    }]
                }, {
                    label: "退出登录",
                    click: () => {
                        console.info("退出登录click");
                    }
                }]
            }
        };
    }
}).$mount("#example");
