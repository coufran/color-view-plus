const routes = [{
    path: "/page",
    component: () => import("./ExamplePage.vue")
}, {
    path: "/*",
    redirect: "/page"
}];

export default routes;