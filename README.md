# ColorViewPlus
ColorViewPlus是基于ColorView的扩展组件库，增加了后台管理页面的基本框架，样式由class指定或通过外部配置定义，可以完全自定义样式。
# 组件
## 页面框架``cvp-frame``
### 内容
- content：框架内容，默认使用``cvp-logo``、``cvp-header``、``cvp-nav``和``cvp-body``填充。
- logoContent：logo部分内容。
- headerContent：header部分内容。
- navContent：nav部分内容。
- bodyContent：body部分内容。
```html
<cvp-frame>[content]</cvp-frame>
<cvp-frame>
    <template #logo>
        [logoContent]
    </template>
    <template #header>
        [headerContent]
    </template>
    <template #nav>
        [navContent]
    </template>
    <template #body>
        [bodyContent]
    </template>
</cvp-frame>
```
### 外观
- type: ``frame``
- color: ``primary``
- size: ``medium``
```html
<cv-frame class="[type] [color] [size]"></cv-frame>
```
### 绑定
- ``logo``：页面logo，详见``cvp-logo``部分。
- ``menu``：页面菜单配置，详见``cvp-nav``部分。
```html
<cv-frame :logo="logo" :menu="menu"></cv-frame>
```

## 页面Logo``cvp-logo``
### 内容
- content：框架Logo，默认使用logo属性绑定的内容。
```html
<cvp-logo>[content]</cvp-logo>
```
### 外观
- type: ``logo``
- color: ``primary``
- size: ``medium``
```html
<cv-logo class="[type] [color] [size]"></cv-logo>
```
### 绑定
- ``logo``：logo为字符串或图片，图片可使用``require``引入。
```html
<cv-logo :logo="logo"></cv-logo>
```

## 页面头部``cvp-header``
### 内容
- content：框架头部，默认使用``cvp-user``组件。
```html
<cvp-header>[content]</cvp-header>
```
### 外观
- type: ``header``
- color: ``primary``
- size: ``medium``
```html
<cv-header class="[type] [color] [size]"></cv-header>
```

## 页面导航``cvp-nav``
### 外观
- type: ``nav``
- color: ``primary``
- size: ``medium``
```html
<cv-nav class="[type] [color] [size]"></cv-nav>
```
### 绑定
- ``menu``：菜单配置，使用形如``{meta: {icon, name, path}, chindren: [...]}``结构。
```html
<cv-nav :menu="menu"></cv-nav>
```

## 页面主体``cvp-body``
### 外观
- type: ``body``
- color: ``primary``
- size: ``medium``
```html
<cv-body class="[type] [color] [size]"></cv-body>
```

## 用户组件``cvp-user``
### 外观
- type: ``user``
- color: ``primary``
- size: ``medium``
```html
<cv-user class="[type] [color] [size]"></cv-user>
```
### 绑定
- ``$root.user``：在root组件下，需要拥有名为``user``、行为``{meta: {username, avatar}, operations: [{label, click: function, operations: [...]}]}``的数据。

## 页面``cvp-page``
### 内容
- content：页面内容。
```html
<cvp-page>[content]</cvp-page>
```
### 外观
- type: ``page``
- color: ``primary``
- size: ``medium``
```html
<cv-page class="[type] [color] [size]"></cv-page>
```

## 页面``cvp-page-header``
### 内容
- content：页面顶部按钮。
```html
<cvp-page-header>[content]</cvp-page-header>
```
### 外观
- type: ``page-header``
- color: ``primary``
- size: ``medium``
```html
<cv-page-header class="[type] [color] [size]"></cv-page-header>
```
### 绑定
- ``title``：页面标题。
```html
<cv-page-header :title="title"></cv-page-header>
```

## 页面``cvp-page-data``
### 内容
- content：页面数据列配置，需要传入``cv-table-column``组件。
```html
<cvp-page-data>[content]</cvp-page-data>
```
### 外观
- type: ``page-data``
- color: ``primary``
- size: ``medium``
```html
<cv-page-data class="[type] [color] [size]"></cv-page-data>
```
### 绑定
- ``data``：Array，页面数据，参考``cv-table``组件。
- ``page``：分页数据，参考``cv-pagination``组件。
```html
<cv-page-data :data="data" :page="page"></cv-page-data>
```
### 事件
- ``page``：页面跳转，事件对象为page对象。
```html
<cv-button @page="search($event)"></cv-button>
```
